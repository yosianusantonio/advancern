/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';


const App:() => Yosianus = () => {
  return (
    <View style={styles.container}>
      <Text>Hello Kelas React Native Lanjutan Sanbercode!</Text>
    </View>
  );
};
 

const styles = StyleSheet.create({
    container : {
  
        flexGrow: 1,
        
        justifyContent:'center',
        
        alignItems: 'center'
        
        },
});

export default App;
