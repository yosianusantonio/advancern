import React,{useContext} from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import {RootContext} from '../Tugas4';

const Todolist = () => {
 
    const state= useContext(RootContext)
    console.log("Todolist -> state", state)

    const Task = ({item,index}) => (
        <View style={styles.taskcont}>
        <View style={styles.taskWrapper}>
            <View>
                <Text>{item.date}</Text>
                <Text style={styles.task}>{item.title}</Text>
            </View>
            <Icon2
                name="trash-o"
                size={30}
                color="black"
                style={{ marginLeft: 'auto' }}
                onPress={()=>state.todelete(item.key)}
            />
        </View>
        </View>
    )
   

    return (
        <View>
            <Text style={{ marginTop: '10%', marginLeft: 10, fontSize: 16, color: 'black' }}>Masukan Todolist</Text>
            <View style={styles.textInputContainer}>
                <TextInput
                    style={styles.textInput}
                    placeholder={'Input Here'}
                    placeholderTextColor="gray"
                    value={state.input}
                    onChangeText={(value)=> state.handleChangeInput(value)}
                    
                />
                <TouchableOpacity onPress={()=>state.add()}>
                    <Icon name='plus' size={20} style={styles.plus} />
                </TouchableOpacity>
            </View>
            <FlatList 
            data={state.todos}
            renderItem={Task}
            />
               
            
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    textInput: {
        height: 50,
        flex: 1,
        color: 'black',
        paddingLeft: 10,
        borderColor: 'black',
        borderWidth: 1
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: 'black',
        marginBottom: 5,
    },
    textInputContainer: {
        flexDirection: 'row',
        alignItems: 'baseline',
        borderColor: 'rgb(222,222,222)',
        padding: 10,

    },
    plus: {
        width: 50,
        height: 50,
        padding: 15,
        marginLeft:5,
        backgroundColor: '#3EC6FF',
        justifyContent: 'center',
        alignContent: 'center'
    },
    taskcont:{
        flex:1,
        justifyContent:'center',
        padding:10
    },
    taskWrapper: {
        flexDirection: 'row',
        borderColor: 'lightgray',
        borderWidth: 4,
        padding:15,
        borderRadius:5
    },
    task: {
        marginTop: 6,
        borderColor: '#F0F0F0',
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black',
    },
    verticalLine: {
        borderBottomColor: 'black',
        marginLeft: 10,
        width: '100%',
        position: 'absolute',
        marginTop: 15
    }
});


export default Todolist