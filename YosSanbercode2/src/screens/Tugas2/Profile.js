import * as React from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    Image,
    StatusBar
    } from 'react-native';

function Hello() {
    return (
    <View style={styles.container}>
         <StatusBar barStyle="dark-content" backgroundColor="#3EC6FF" />
      <View style={styles.headprf}>
      <Image style={styles.pp} source={require('../../assets/pp.jpg')}/>
      <Text style={styles.textname}>Yosianus Antonio</Text>
      <View style={styles.bio}>
          <View style={styles.setbio}>
          <Text>Tanggal Lahir</Text>
          <Text>11 Juni 1998</Text>
          </View>
          <View style={styles.setbio}>
          <Text>Jenis Kelamin</Text>
          <Text>Laki - laki</Text>
          </View>
          <View style={styles.setbio}>
          <Text>Hobi</Text>
          <Text>Ngoding, Musik, Baca</Text>
          </View>
          <View style={styles.setbio}>
          <Text>No.Tlp</Text>
          <Text>081278757447</Text>
          </View>
          <View style={styles.setbio}>
          <Text>Email</Text>
          <Text>yosianusantonio@gmail.com</Text>
          </View>
          <View style={styles.setbio}>
          <Text>Pekerjaan</Text>
          <Text>Masih Nyari :D</Text>
          </View>
      </View>
      </View>
      
    </View>
  );
};
 

const styles = StyleSheet.create({
    container : {
        flexGrow: 1,
        alignItems: 'center'
        },
    headprf:{
        width: 400, 
        height: 280, 
        backgroundColor: '#3EC6FF',
        alignItems:'center',
    },
    pp:{
        marginTop:50,
        width:100,
        height:100,
        borderRadius:100
    },
    textname:{
        marginTop:5,
        color:"white",
        fontSize:20,
        fontWeight:'bold'
    },
    bio:{
        marginTop: 50,
        width: 350, 
        borderRadius: 8,
        shadowRadius: 0.25,
        shadowOpacity: 4,
        shadowColor: 'black',
        elevation: 5,
        backgroundColor: 'white',
    },
    setbio:{
        padding:15,
        flexDirection:'row',
        alignContent:'center',
        justifyContent:'space-between'
    }
});

export default Hello;