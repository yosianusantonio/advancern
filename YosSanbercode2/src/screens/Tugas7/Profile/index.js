import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import api from '../../../api'
import { GoogleSignin } from '@react-native-community/google-signin';

function Profile({ navigation }) {

    const [userInfo, setUserInfo] = useState(null)

    useEffect(() => {
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("token")
                return getVenue(token)
                console.log("getToken ->", token)
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getCurrentUser()
    }, [userInfo])

   

    const getCurrentUser = async()=>{
        const userInfo = await GoogleSignin.signInSilently()
        console.log("getCurrentUser -> userInfo", userInfo)
        setUserInfo(userInfo)
    }

    const getVenue = (token) => {
        Axios.get(`${api}/venues`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                console.log("Profile -> res", res)
            })
            .catch((err) => {
                console.log("Profile -> err", err)
            })
    }

    const onLogout = async () => {
        try {
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
            await AsyncStorage.removeItem("token")
            navigation.navigate('Login')
        }
        catch (err) {
            console.log(err)
        }

    }


    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#3EC6FF" />
            <View style={styles.headprf}>
                <Image style={styles.pp} source={{uri: userInfo && userInfo.user && userInfo.user.photo}} />
                <Text style={styles.textname}>{userInfo && userInfo.user && userInfo.user.name}</Text>
                </View>
                <View style={styles.headcvr}>
                   
                <View style={styles.bio}>
                    <View style={styles.setbio}>
                        <Text>Tanggal Lahir</Text>
                        <Text>11 Juni 1998</Text>
                    </View>
                    <View style={styles.setbio}>
                        <Text>Jenis Kelamin</Text>
                        <Text>Laki - laki</Text>
                    </View>
                    <View style={styles.setbio}>
                        <Text>Hobi</Text>
                        <Text>Ngoding, Musik, Baca</Text>
                    </View>
                    <View style={styles.setbio}>
                        <Text>No.Tlp</Text>
                        <Text>081278757447</Text>
                    </View>
                    <View style={styles.setbio}>
                        <Text>Email</Text>
                        <Text>{userInfo && userInfo.user && userInfo.user.email}</Text>
                    </View>
                    <View style={styles.setbio}>
                        <Text>Pekerjaan</Text>
                        <Text>Masih Nyari :D</Text>
                    </View>
                    <View>
                    <TouchableOpacity style={styles.buttonmasuk} onPress={() => onLogout()}>
                        <Text style={styles.buttonText}>Logout</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flex:1,
        alignContent:'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    headprf: {
        width:400,
        backgroundColor: '#3EC6FF',
        alignItems: 'center'
    },
    headcvr: {
        width:400,
        
        alignItems: 'center'
    },
    pp: {
        marginTop: 50,
        width: 100,
        height: 100,
        borderRadius: 100
    },
    textname: {
        marginTop: 5,
        color: "white",
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom:100
    },
    bio: {
        bottom:80,
        width: 350,
        borderRadius: 8,
        shadowRadius: 0.25,
        shadowOpacity: 4,
        shadowColor: 'black',
        elevation: 5,
        backgroundColor: 'white',
    },
    setbio: {
        padding: 15,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-between'
    },
    buttonmasuk: {
        width: 320,
        backgroundColor: '#3EC6FF',
        borderRadius: 10,
        marginVertical: 10,
        paddingVertical: 8,
        alignSelf: 'center',
    },
    buttonText: {
        fontSize: 14,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }
});

export default Profile;