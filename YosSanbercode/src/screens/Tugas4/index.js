import React, { useState, createContext } from 'react'
import Todolist from './Todolist'

export const RootContext = createContext();

const Context = () => {

    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])


    const handleChangeInput = (value) => {
        setInput(value)
    }

    const add = () => {
        const today = new Date()
        const date = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();
        setTodos([...todos, { 
            title: input, date:date, key:Date.now(), checked:false
        }])
        setInput('')
    }
 
    const todelete = (id) => {
        setTodos(todos.filter((todo) => {
            if (todo.key !== id) return true
        })
        )
    }


    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            add,
            todelete
        }}>
            <Todolist />
        </RootContext.Provider>
    )
}
export default Context
