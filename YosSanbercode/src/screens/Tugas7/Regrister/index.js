import React, { useState } from 'react';
import { View, Text, Image, Alert, Modal, StatusBar, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { RNCamera } from 'react-native-camera';
import storage from '@react-native-firebase/storage'
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

function Register({ navigation }) {

    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)
    

    const toggleCamera =()=>{
        setType(type === 'back' ? 'front':'back' )
    }

    const takePicture = async () =>{
        
        const options={quality:0.5, base64:true}
        if (camera){
            const data = await camera.takePictureAsync(options)
            console.log("takePicture -> data ", data)
            setPhoto(data)
            setIsVisible(false)
        }else{
            console.log("eror")
        }
    }

    const uploadImange = (uri)=>{
        const sessionId = new Date().getTime()
        return storage()
        .ref(`images/${sessionId}`)
        .putFile(uri)
        .then((response)=>{
            alert('Upload Success')
        })
        .catch((error)=>{
            alert(error)
        })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={ref => {
                             camera = ref;
                        }}
                        type={type}
                    >  
                    <View style={styles.btnFlipContainer}>
                           <TouchableOpacity style={styles.btnFlip} onPress={()=> toggleCamera()}>
                               <MaterialCommunity name='rotate-3d-variant' size={25} />
                           </TouchableOpacity>
                       </View>
                       <View style={styles.round}/>
                       <View style={styles.rectangle}/>
                       <View style={styles.btnTakeContainer}>
                           <TouchableOpacity style={styles.btnTake} onPress={()=> takePicture()}>
                               <Icon name="camera" size={30}/>
                           </TouchableOpacity>
                       </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.headprf}>
            <Image style={styles.pp} source={photo === null ? require('../../../assets/user.png'):{uri: photo.uri}} />
                <TouchableOpacity style={styles.ChangePicture} onPress={() => setIsVisible(true)}>
                    <Text style={styles.btnChangePicture}>Change picture</Text>
                </TouchableOpacity>
            </View>
            {renderCamera()}
            <View style={styles.headcvr}>
                <View style={styles.bio}>
                    <View style={styles.forminput} >
                        <Text style={styles.formtext}>Name</Text>
                        <TextInput style={styles.input} placeholder="Name" />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Email</Text>
                        <TextInput style={styles.input}  placeholder="Email" />
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}  placeholder="Password" />
                    </View>
                    <View>
                        <TouchableOpacity style={styles.buttonmasuk} onPress={()=> uploadImange(photo.uri)}>
                            <Text style={styles.buttonText} >Regrister</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    headprf: {
        width: 400,
        backgroundColor: '#3EC6FF',
        alignItems: 'center'
    },
    btnChangePicture:{
        color:'white',
        fontWeight:'bold',
        fontSize:14
    },
    headcvr: {
        width: 400,
        alignItems: 'center',
        marginTop:30
    },
    pp: {
        marginTop:50,
        width: 100,
        height: 100,
        borderRadius: 100,
        borderWidth:1
    },
    ChangePicture: {
        marginTop:20,
        width: 100,
        height: 100,
    },
    bio: {
        bottom: 80,
        width: 350,
        borderRadius: 8,
        shadowRadius: 0.25,
        shadowOpacity: 4,
        shadowColor: 'black',
        elevation: 5,
        backgroundColor: 'white',
    },
    forminput: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input: {
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor:'#003366'
    },
    formtext: {
        color: '#003366',
    },
    buttonmasuk: {
        width: 320,
        backgroundColor: '#3EC6FF',
        borderRadius: 10,
        marginVertical: 10,
        paddingVertical: 8,
        alignSelf: 'center',
    },
    buttonText: {
        fontSize: 14,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    btnFlipContainer:{
        flex:1,
        margin:20
    },
    btnFlip:{
        width:50,
        height:50,
        backgroundColor:'lightgrey',
        borderRadius:100,
        justifyContent:"center",
        alignItems:"center"
    },
    btnTakeContainer:{
        flex:1,
        margin:20,
        justifyContent:"center",
        alignItems:"center"
    },
    btnTake:{
        width:100,
        height:100,
        backgroundColor:'lightgrey',
        borderRadius:100,
        justifyContent:"center",
        alignItems:"center"
    },
    round:{
        bottom:100,
        width:150,
        height:230,
        borderWidth:1,
        borderColor:"white",
        alignSelf:'center',
        borderRadius:100,
        
    },
    rectangle:{
        width:150,
        height:100,
        borderWidth:1,
        borderColor:"white",
        alignSelf:'center',
    }
});



export default Register;