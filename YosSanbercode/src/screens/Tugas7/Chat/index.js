import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    ScrollView,
    Dimensions
} from 'react-native';

import { GiftedChat, Avatar } from 'react-native-gifted-chat'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'

//user : yos@gmail.com pw : 11061998
//user: yefta@gmail.comm pw : 11051994

const App = ({ navigation }) => {

    const [messages, setMessages]=useState([])
    const [user, setUser]=useState({})

    useEffect(() => {
        const user = auth().currentUser;
        setUser(user)
        console.log("Chat -> user", user)
        getData()
        return () => {
            const db = database().ref('messages')
            if(db){
                db.off()
            }
        }
    }, [])

    const getData = () =>{
        database().ref('messages').limitToLast(20).on('child_added',snapshot =>{
            const value = snapshot.val()
            setMessages(previousMessages => GiftedChat.append(previousMessages,value))
            console.log("getData -> value", value)
        })
    }

    const onSend =((messages =[])=>{
        for(let i=0;i< messages.length;i++){
            database().ref('messages').push({
                _id: messages[i]._id,
                createAt: database.ServerValue.TIMESTAMP,
                text: messages[i].text,
                user: messages[i].user
            })
        }
        console.log("onSend -> messages", messages)
    })



    return (
        <GiftedChat
        messages={messages}
        onSend={messages => onSend(messages)}
            user={{
                _id: user.uid,
                name: user.email,
                avatar: "https://yosianusantonio.github.io/assets/img/pp.jpg"

            }}
        />
    );
}

const styles = StyleSheet.create({
    container: {
        height: Dimensions.get('screen').height,
        marginTop: "5%",
        alignItems: 'center'
    }
});


export default App