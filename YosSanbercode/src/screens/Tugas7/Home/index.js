import React, { useState,useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    ScrollView,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

const App = ({navigation}) => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.kelas}>
                    <View style={styles.headerkls}>
                        <Text style={styles.klstext}>Kelas</Text>
                    </View>
                    <View style={styles.klsDirec}>
                        <TouchableOpacity onPress={()=> navigation.navigate('React Native', {name:'Yos'})}>
                        <View style={styles.klsname}>
                            <Icon style={styles.icon} name='logo-react' size={60} />
                            <Text style={styles.textwht}>React native</Text>
                        </View>
                        </TouchableOpacity>
                        <View style={styles.klsname}>
                            <Icon style={styles.icon} name='logo-python' size={60} />
                            <Text style={styles.textwht}>Python</Text>
                        </View>
                        <View style={styles.klsname}>
                            <Icon style={styles.icon} name='logo-react' size={60} />
                            <Text style={styles.textwht}>React-Js</Text>
                        </View>
                        <View style={styles.klsname}>
                            <Icon style={styles.icon} name='logo-laravel' size={60} />
                            <Text style={styles.textwht}>Laravel</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.kelas}>
                    <View style={styles.headerkls}>
                        <Text style={styles.klstext}>Kelas</Text>
                    </View>
                    <View style={styles.klsDirec}>
                        <View style={styles.klsname}>
                            <Icon style={styles.icon} name='logo-wordpress' size={60} />
                            <Text style={styles.textwht}>Wordpress</Text>
                        </View>
                        <View style={styles.klsname}>
                            <Icon2 style={styles.icon} name='pencil-ruler' size={60} />
                            <Text style={styles.textwht}>Design Grafis</Text>
                        </View>
                        <View style={styles.klsname}>
                            <Icon2 style={styles.icon} name='server' size={60} />
                            <Text style={styles.textwht}>Web Server</Text>
                        </View>
                        <View style={styles.klsname}>
                            <Icon2 style={styles.icon} name='page-layout-header-footer' size={60} />
                            <Text style={styles.textwht}>UI/UX Design</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.summary}>
                    <View style={styles.headersum}>
                        <Text style={styles.klstext}>Summary</Text>
                    </View>
                    <View>
                        <Text style={styles.klstext}>React native</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>Python</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>React-Js</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>Laravel</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>Wordpress</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>Design Grafis</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>Web Server</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.klstext}>UI/UX Design</Text>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Today</Text>
                            <Text style={styles.subtext}>20 Orang</Text>
                        </View>
                        <View style={styles.subsum}>
                            <Text style={styles.subtext}>Total</Text>
                            <Text style={styles.subtext}>100 Orang</Text>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        height :Dimensions.get('screen').height,
        marginTop: "5%",
        alignItems: 'center'
    },
    kelas: {
        width: '90%',
        height: '15%',
        borderRadius: 10,
        backgroundColor: '#3EC6FF',
        alignItems: "center",
        marginBottom: 10,
    },
    headerkls: {
        width: '100%',
        height: '18%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#088dc4',
    },
    klstext: {
        color: "#ffffff",
        marginLeft: "3%",
        marginRight: '3.5%'
    },
    klsDirec: {
        flexDirection: 'row',
        margin: '1.5%'
    },
    icon: {
        color: "#ffffff",
        alignSelf: 'center'
    },
    klsname: {
        flex: 1,
        justifyContent: 'space-between',
    },
    textwht: {
        color: "#ffffff",
        alignSelf: 'center'
    },
    summary: {
        width: '90%',
        borderRadius: 10,
        backgroundColor: '#3EC6FF',
        paddingBottom:'5%'
    },
    headersum: {
        width: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: '#088dc4',
    },
    subsum: {
        width: '100%',
        backgroundColor: '#088dc4',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    subtext: {
        color: "#ffffff",
        marginLeft: "15%",
        marginRight: '15%'
    }
});


export default App