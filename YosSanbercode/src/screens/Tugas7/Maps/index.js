import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Dimensions
} from 'react-native';
import Maps from '@react-native-mapbox-gl/maps'
import MapboxGL from "@react-native-mapbox-gl/maps";

MapboxGL.setAccessToken(`pk.eyJ1IjoieW9zaWFudXMiLCJhIjoiY2tldGg0cmR4MWN1OTMwcXgwd3htOW9kdyJ9.1C3PNevaxkDD7lcmM-0lqQ`)

const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]


const App = () => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapboxGL.requestAndroidLocationPermissions()
            } catch (error) {
                console.log(error)
            }
        }
        getLocation()
    }, [])

    return (
        <View style={styles.container}>
            <MapboxGL.MapView
                style={{ flex: 1 }}>

                <MapboxGL.UserLocation
                    visible={true} />

                <MapboxGL.Camera
                    followUserLocation={true} />

                {coordinates.map((item, index) => {
                    return (
                        <MapboxGL.PointAnnotation
                            id={`pointAnnotation ${index}`}
                            key={index}
                            coordinate={item}>
                            <MapboxGL.Callout
                                key={index}
                                title={`Longtitude: ${item[0]} \n Latitude:${item[1]}`} />
                        </MapboxGL.PointAnnotation>
                    )
                })}
            </MapboxGL.MapView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height:"100%",
        width: "100%",

    }
});


export default App