import React,{ useState, useEffect } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  Alert

} from 'react-native';

import {CommonActions} from '@react-navigation/native'

import Axios from 'axios'
import api from '../../api/index'
import auth from '@react-native-firebase/auth'
import TouchID from 'react-native-touch-id'
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'



const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErorDescription: 'Failed',
  cencelText: 'Cancel'
}

const Login = ({ navigation }) => {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')


  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem("token", token)
    }
    catch (err) {
      console.log(err)
    }
  }
  useEffect(() => {
    configureGoogleSignIn()
  }, [])

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId: '572842171951-o4rl00jhp5mte1u37ejdjmf0a7houi25.apps.googleusercontent.com'
    }, [])
  }

  const signInWithGoogle = async () => {
    try {
      const { idToken } = await GoogleSignin.signIn()
      console.log("signInWithGoogle -> idToken", idToken)

      const credential = auth.GoogleAuthProvider.credential(idToken)

      auth().signInWithCredential(credential)

      navigation.navigate('TabsStackScreen')
    } catch (error) {
      console.log("signWithGoogle -> error", error)
      Alert.alert('SignIn Failed !!')

    }
  }

  const onLoginPress=()=>{
    navigation.dispatch(
      CommonActions.reset({
        index:0, 
        routes:[{name:'TabsStackScreen'}]
      })
    )
  }

  // const onLoginPress=()=>{
  //   return auth().signInWithEmailAndPassword(email,password)
  //   .then((res)=>{
  //     navigation.navigate('TabsStackScreen')
  //   })
  //   .catch((err)=>{
  //     console.log("onLoginPress ->", err)
  //   })
  // }


  // const onLoginPress = () => {
  //   let data = {
  //     email: email,
  //     password: password
  //   }
  //   Axios.post(`${api}/login`, data, {
  //     timeout: 20000
  //   })
  //     .then((res) => {
  //       console.log("Login -> res", res)
  //       saveToken(res.data.token)
  //       navigation.navigate('TabsStackScreen')
  //     })
  //     .catch((err) => {
  //       console.log("login -> err", err)
  //       Alert.alert('Wrong Username or Password !!')
  //     })

  // }

  const signInWithFingerprint = () => {
    TouchID.authenticate('',  config)
    .then( success => {
      alert("Authetication Success")
      navigation.navigate('TabsStackScreen')
    })
    .catch( error => {
      alert("Authetication Failed")
    })
  }


  return (


    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('../../assets/images/logo.jpg')}
      />

      <Text style={styles.leftAlign}></Text>
      <TextInput style={styles.inputBox}

        placeholder="Username/Email"
        selectionColor="#fff"
        keyboardType="email-address"
        value={email}
        onChangeText={(email) => setEmail(email)}
      />

      <TextInput style={styles.inputBox}
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={(password) => setPassword(password)}

      />

      <TouchableOpacity style={styles.buttonmasuk} onPress={() => onLoginPress()}>

        <Text style={styles.buttonText}>Login</Text>

      </TouchableOpacity>

      <Text style={styles.questionlogin}>OR</Text>

      <GoogleSigninButton
      onPress={()=>signInWithGoogle()}
        style={{ width: '80%' }}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark} />

      <TouchableOpacity style={styles.buttonfinger} onPress={() => signInWithFingerprint()}>
        <Text style={styles.buttonText}>Sign In With Fingerprint</Text>
      </TouchableOpacity>

      <View style={{flexDirection:'row', marginTop:50}}> 
      <Text style={{color:'black'}}>Belum Punya Akun ? </Text>
      <TouchableOpacity onPress={()=> navigation.navigate('Regrister')}>
      <Text style={{fontWeight:'bold'}}> Buat Akun </Text>
      </TouchableOpacity>
      </View>
      
    </View>

  )
}


const styles = StyleSheet.create({

  container: {

    flexGrow: 1,

    justifyContent: 'center',

    alignItems: 'center',

    backgroundColor: '#ffffff'

  },

  questionlogin: {
    fontSize: 13,
    margin: 10,
    color: '#3EC6FF'
  },


  inputBox: {

    width: 300,

    borderRadius: 35,

    height: 45,

    borderWidth: 1,

    borderColor: '#1c313a',

    backgroundColor: 'rgba(255, 255,255,0.2)',

    paddingHorizontal: 16,

    fontSize: 16,

    color: '#000000',

    marginVertical: 10

  },


  buttonmasuk: {

    width: '80%',

    backgroundColor: '#3EC6FF',

    borderRadius: 5,

    marginVertical: 10,

    paddingVertical: 9

  },

  buttonfinger: {

    width: '79%',

    backgroundColor: '#1c313a',

    borderRadius: 5,

    marginVertical: 10,

    paddingVertical: 8

  },

  buttonText: {

    fontSize: 16,

    fontWeight: '500',

    color: '#ffffff',

    textAlign: 'center'
  },
  image: {
    padding: 10
  }
});

export default Login
