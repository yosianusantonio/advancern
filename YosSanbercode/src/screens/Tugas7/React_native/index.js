import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    processColor
} from 'react-native';

import {BarChart} from 'react-native-charts-wrapper'

const valum =[{y:[100, 40]},{y:[80, 60]},{y:[40, 90]},{y:[78, 45]},{y:[67, 87]},{y:[98, 32]},{y:[150, 90]},]

const data = [
    {y:[100, 40], marker: ["React Native Dasar"+"\n"+valum[0].y[0], "React Native Lanjutan"+"\n"+valum[0].y[1]]},
    {y:[80, 60], marker: ["React Native Dasar"+"\n"+valum[1].y[0], "React Native Lanjutan"+"\n"+valum[1].y[1]]},
    {y:[40, 90], marker: ["React Native Dasar"+"\n"+valum[2].y[0], "React Native Lanjutan"+"\n"+valum[2].y[1]]},
    {y:[78, 45], marker: ["React Native Dasar"+"\n"+valum[3].y[0], "React Native Lanjutan"+"\n"+valum[3].y[1]]},
    {y:[67, 87], marker: ["React Native Dasar"+"\n"+valum[3].y[0], "React Native Lanjutan"+"\n"+valum[4].y[1]]},
    {y:[98, 32], marker: ["React Native Dasar"+"\n"+valum[4].y[0], "React Native Lanjutan"+"\n"+valum[5].y[1]]},
    {y:[150, 90], marker: ["React Native Dasar"+"\n"+valum[6].y[0], "React Native Lanjutan"+"\n"+valum[6].y[1]]},
]



const react_native = ({navigation}) => {

    const [legend, setLegend] = useState({
        enabled: false,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
 })

const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: data,
                label: '',
                config: {
                    colors: [processColor('#3EC6FF'), processColor('#1c313a')],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
 })
const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
})
const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
 })

    return (
        <View style={styles.container}>
           <BarChart
           style={{flex:1, margin:'3%'}}
           data={chart.data}
           yAxis={yAxis}
           xAxis={xAxis}
           pinchZoom={false}
           doubleTapToZoomEnabled={false}
           chartDescription={{text:''}}
           legend={legend}
           marker={{
               enabled:true,
               markerColor: 'grey',
               textColor:'white',
               textSize:14
           }}
           />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height:"100%",
        width: "100%",
        backgroundColor:"#ffffff"

    }
});


export default react_native