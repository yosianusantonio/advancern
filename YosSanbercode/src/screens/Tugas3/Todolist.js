import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/FontAwesome';

const App = () => {
    const [value, setValue] = useState('')
    const [todos, setTodos] = useState([])
    var today = new Date()
    var date = today.getDate() + "/" + parseInt(today.getMonth() + 1) + "/" + today.getFullYear();

    const add = () => {
        setTodos([...todos, {
            text: value,date:date, key:Date.now() , checked: false
        }])
        setValue('')
    }

    const todelete = (id) => {
        setTodos(todos.filter((todo) => {
            if (todo.key !== id) return true
        })
        )
    }

    const Task = (props) => (
        <View style={styles.taskcont}>
        <View style={styles.taskWrapper}>
            <View>
                {props.checked && <View style={styles.verticalLine}></View>}
                <Text>{props.date}</Text>
                <Text style={styles.task}>{props.text}</Text>
            </View>
            <Icon2
                name="trash-o"
                size={30}
                color="black"
                style={{ marginLeft: 'auto' }}
                onPress={props.delete}
            />
        </View>
        </View>
    )


    return (
        <View>
            <Text style={{ marginTop: '10%', marginLeft: 10, fontSize: 16, color: 'black' }}>Masukan Todolist</Text>
            <View style={styles.textInputContainer}>
                <TextInput
                    style={styles.textInput}
                    multiline={true}
                    onChangeText={(value) => setValue(value)}
                    placeholder={'Input Here'}
                    placeholderTextColor="gray"
                    value={value}
                />
                <TouchableOpacity onPress={() => add()}>
                    <Icon name='plus' size={20} style={styles.plus} />
                </TouchableOpacity>
            </View>
            <ScrollView style={{ width: '100%' }}>
                {todos.map((task) => (
                    <Task
                        text={task.text}
                        key={task.key}
                        delete={() => todelete(task.key)}
                        date={task.date}
                    />
                ))
                }
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    textInput: {
        height: 50,
        flex: 1,
        color: 'black',
        paddingLeft: 10,
        borderColor: 'black',
        borderWidth: 1
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: 'black',
        marginBottom: 5,
    },
    textInputContainer: {
        flexDirection: 'row',
        alignItems: 'baseline',
        borderColor: 'rgb(222,222,222)',
        padding: 10,

    },
    plus: {
        width: 50,
        height: 50,
        padding: 15,
        marginLeft:5,
        backgroundColor: '#3EC6FF',
        justifyContent: 'center',
        alignContent: 'center'
    },
    taskcont:{
        flex:1,
        justifyContent:'center',
        padding:10
    },
    taskWrapper: {
        flexDirection: 'row',
        borderColor: 'lightgray',
        borderWidth: 4,
        padding:15,
        borderRadius:5
    },
    task: {
        marginTop: 6,
        borderColor: '#F0F0F0',
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black',
    },
    verticalLine: {
        borderBottomColor: 'black',
        marginLeft: 10,
        width: '100%',
        position: 'absolute',
        marginTop: 15
    }
});


export default App