import React, { useState, useEffect } from 'react';
import { View, Text, Image, StatusBar, StyleSheet } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage'
import Login from '../Tugas7/Login'

//data yang akan digunakan dalam onboarding
const slides = [
    {
        key: 1,
        title: 'Belajar Intensif',
        text: '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
        image: require('../../assets/images/working-time.png')
    },
    {
        key: 2,
        title: 'Teknologi Populer',
        text: 'Menggunakan bahasa pemrograman populer',
        image: require('../../assets/images/research.png'),
    },
    {
        key: 3,
        title: 'From Zero to Hero',
        text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
        image: require('../../assets/images/venture.png'),
    },
    {
        key: 4,
        title: 'Training Gratis',
        text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
        image: require('../../assets/images/money-bag.png'),
    }
];

const Intro = ({ navigation }) => {

    const [mainApp, setMainApp] = useState(false)
    const [onBoard, setOnboard] = useState(true)

    useEffect(() => {
        AsyncStorage.getItem('first_time').then((value) => {
            setMainApp(!!value),
                setOnboard(false)
        });
    })
    //menampilkan data slides kedalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    }

    //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
    const onDone = () => {
        AsyncStorage.setItem('first_time', 'true').then(() => {
            setMainApp(true)
            navigation.navigate('Login')
        });
    }

    //mengcustom tampilan button done
    const renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                    style={{}}
                />
            </View>
        );
    };

    const login =()=>{
        navigation.navigate('Login')
    }
    //mengcustom tampilan next button
    const renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="arrow-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };
    if (mainApp) {
    return (<View>{login()}</View>)
    } else {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
                <View style={{ flex: 1 }}>
                    {/* merender atau menjalankan library react-native-app-intro-slider */}
                    <AppIntroSlider
                        data={slides}
                        onDone={onDone}
                        renderItem={renderItem}
                        renderDoneButton={renderDoneButton}
                        renderNextButton={renderNextButton}
                        keyExtractor={(item, index) => index.toString()}
                        activeDotStyle={{ backgroundColor: '#191970' }}
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#191970',
        alignSelf: 'center',
        margin: 20
    },
    text: {
        fontSize: 16,
        textAlign: 'center',
        margin: 20,
        color: 'grey'
    },
    buttonCircle: {
        width: 50,
        height: 50,
        backgroundColor: '#191970',
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default Intro
