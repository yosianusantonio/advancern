import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicon from 'react-native-vector-icons/Ionicons';
import SplashScreen from './SplashScreen';
import Intro from './Intro';
import Login from '../Tugas7/Login';
import Profile from '../Tugas7/Profile';
import Home from '../Tugas7/Home';
import Regrister from '../Tugas7/Regrister'
import Maps from '../Tugas7/Maps'
import react_native from '../Tugas7/React_native';
import Chat from '../Tugas7/Chat';

const Stack = createStackNavigator();
const TabsStack = createBottomTabNavigator();

const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, color, size }) => {
            if (route.name == 'Home') {
                return <FontAwesome5 name={'home'} size={size} color={color} solid />
            } else if (route.name == 'Profile') {
                return <FontAwesome5 name="user-circle" size={size} color={color} solid />
            }else if (route.name == 'Maps') {
                return <FontAwesome5 name="map-marked-alt" size={size} color={color} solid />
            }else if (route.name == 'Chat') {
                return <Ionicon name="chatbubbles-sharp" size={size} color={color} solid />
            }
        },
    });
}


const TabsStackScreen = () => (
    <TabsStack.Navigator screenOptions={iconTab}>
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Maps" component={Maps} options={{ headerShown: false }} />
        <Stack.Screen name="Chat" component={Chat} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
    </TabsStack.Navigator>
);

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Regrister" component={Regrister} options={{ headerShown: false }} />
        <Stack.Screen name="React Native" component={react_native} />
        <Stack.Screen name='TabsStackScreen' component={TabsStackScreen} options={{ headerShown: false }}/>
    </Stack.Navigator>
)

function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)

    //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation;