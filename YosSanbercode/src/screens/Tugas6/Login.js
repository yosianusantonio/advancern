import React, { Component } from 'react';

import {

StyleSheet,
Text,
View,
TextInput,
TouchableOpacity,
Image

} from 'react-native';



const Intro =()=> {



return(


<View style={styles.container}>
<Image
        style={styles.image}
        source={require('../../assets/images/logo.png')}
      />

<Text style={styles.leftAlign}></Text>
<TextInput style={styles.inputBox}

placeholder="Username/Email"

selectionColor="#fff"

keyboardType="email-address"

onSubmitEditing={()=> this.password.focus()}

/>

<TextInput style={styles.inputBox}


placeholder="Password"

secureTextEntry={true}



ref={(input) => this.password = input}

/>

<TouchableOpacity style={styles.buttonmasuk}>

<Text style={styles.buttonText}>Masuk</Text>

</TouchableOpacity>

<Text style={styles.questionlogin}>Atau</Text>

<TouchableOpacity style={styles.buttondaftar}>

<Text style={styles.buttonText}>Daftar ?</Text>

</TouchableOpacity>

</View>

)
}


const styles = StyleSheet.create({

container : {

flexGrow: 1,

justifyContent:'center',

alignItems: 'center',

backgroundColor:'#ffffff'

},

questionlogin:{
fontSize:20,
color:'#3EC6FF'
},


inputBox: {

width:300,

borderRadius:35,

height:45,

borderWidth:1,

borderColor :'#1c313a',

backgroundColor:'rgba(255, 255,255,0.2)',

paddingHorizontal:16,

fontSize:16,

color:'#000000',

marginVertical: 10

},

buttondaftar: {

width:200,

backgroundColor:'#1c313a',

borderRadius: 25,

marginVertical: 10,

paddingVertical: 13

},

buttonmasuk: {

  width:200,
  
  backgroundColor:'#3EC6FF',
  
  borderRadius: 25,
  
  marginVertical: 10,
  
  paddingVertical: 13
  
  },

buttonText: {

fontSize:16,

fontWeight:'500',

color:'#ffffff',

textAlign:'center'
}
});

export default Intro